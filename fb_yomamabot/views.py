from django.views import generic
from django.http.response import HttpResponse

from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator

import pprint, json, requests, re, random

pp = pprint.PrettyPrinter(indent=4)
PAGE_ACCESS_TOKEN = 'EAAFhyw45JaUBADwAsZCXlRx1C1SLl60M6pL1r2YfcdJVDES6ldZCiHw0Ketdm2tu1IeO4sAaA1mKgKelwgh' \
                    '9EpZAgPxojZC1AbbL9noUTGF5PEbAXLyCIfNj0Nu3s7ALQslLz8w3zvsSkdJxbxtYzfOeVZCZB5bfCPVIK7Q' \
                    'nUAcwZDZD'
POST_MESSAGE_URL = 'https://graph.facebook.com/v2.6/me/messages?access_token=%s'%(PAGE_ACCESS_TOKEN)
VERIFY_TOKEN = '1e6ba3de72d72669'

class YoMamaBotView(generic.View):
    def get(self, request, *args, **kwargs):
        if self.request.GET['hub.verify_token'] == VERIFY_TOKEN:
            return HttpResponse(self.request.GET['hub.challenge'])
        else:
            return HttpResponse('Error, invalid token')

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return generic.View.dispatch(self, request, *args, **kwargs)

    # Post function to handle Facebook messages
    # Read incoming message and call response function
    def post(self, request, *args, **kwargs):
        # Converts the text payload into a python dictionary
        incoming_message = json.loads(self.request.body.decode('utf-8'))
        # Facebook recommends going through every entry since they mighe send
        # multiple messages in a single call during high load
        pp.pprint(incoming_message)
        for entry in incoming_message['entry']:
            for message in entry['messaging']:
                # Check to make sure the received call is a message call
                # This might be delivery, optin, postback for other events
                if 'message' in message:
                    if ('text' in message['message']) and ('quick_reply' not in message['message']):
                        # Then it is a text response.
                        # Print message to the terminal
                        # pp.pprint(message)
                        # Assuming the sender only sends text. Non-text messages like stickers, audio, pictures
                        # are sent as attachments and must be handled accordingly.
                        # post_facebook_message(message['sender']['id'], message['message']['text'])
                        # print('Got message: ' + str(message['message']))
                        respond_to_user_message(message['sender']['id'], message['message'])
                    elif 'quick_reply' in message['message']: # Then it is a quieck reply
                        pp.pprint('Got a quick reply.')
                        respond_to_user_quick_reply(message['sender']['id'], message['message']['quick_reply'])
                elif 'postback' in message: # Read postback if user responds by pressing buttons
                    pp.pprint('Got a postback.')
                    # pp.pprint(message)
                    respond_to_user_postback(message['sender']['id'], message['postback'])

        return HttpResponse()

##############################################
#                                            #
#       Incoming Message Interpreter         #
#                                            #
##############################################


def respond_to_user_message(fbid, message):
    if 'text' in message:  # Make sure the expected message is defined
        outgoing_message = {"recipient": {"id": fbid}, "message": {}}
        build_welcome_message(outgoing_message)

    # Articulate a response depending on what is received.


def respond_to_user_postback(fbid, postback):
    if 'payload' in postback:  # Make sure the expected postback is defined
        payload = postback['payload']
        outgoing_message = {"recipient": {"id": fbid}, "message": {}}
        process_network_command(payload, outgoing_message)


def process_network_command(payload, outgoing_message):
    payload = payload.replace("'", "\"")
    payload = json.loads(payload)
    process = payload['process']
    next_step = payload['next_step']
    data = payload['data']

    function_hash_map[process][next_step](outgoing_message, data)


def respond_to_user_quick_reply(fbid, quick_reply):
    if 'payload' in quick_reply:  # Make sure the expected payload is defined
        payload = quick_reply['payload']
        outgoing_message = {"recipient": {"id": fbid}, "message": {}}
        process_network_command(payload, outgoing_message)


################################################
################################################
#                                             ##
#        Outgoing Message Constructor         ##
#                                             ##
################################################
################################################


def build_welcome_message(outgoing_message):
    outgoing_message["message"]["attachment"] = {
        "type": "template",
        "payload": {
            "template_type": "generic",
            "elements": [
                {
                    "title": "Bienvenido a CrediFast!",
                    "image_url": "http://www.charlottestories.com/wp-content/uploads/2016/04/jimmy-johns-1-dollar"
                                 "-subs.jpg",
                    "subtitle": "Selecciona una de las siguientes opciones:",
                    "buttons": [
                        {
                            "type": "postback",
                            "title": "Aplicar a Tarjetas",
                            "payload": "{'process':'credit_card_application', 'next_step':'previous_credit', 'data':{}}"
                        },
                        {
                            "type": "postback",
                            "title": "Mis Aplicaciones",
                            "payload": "aplicaciones"
                        },
                        {
                            "type": "postback",
                            "title": "Menu Principal",
                            "payload": "menu"
                        }
                    ]
                }
            ]
        }
    }
    send_response(outgoing_message)

##############################################
#                                            #
#          Credit Card Application           #
#                                            #
##############################################


def application_previous_credit(outgoing_message, data):
    # print ('Do you have credit?')
    # Articulate a response depending on what is received.
    outgoing_message['message']['text'] = 'Ya posees una tarjeta de crédito?'
    outgoing_message['message']['quick_replies'] = [
        {
            "content_type": "text",
            "title": "Si",
            "payload": "{'process':'credit_card_application', 'next_step':'choose_benefit', "
                       "'data':{'previous_credit':'true'}}",
            "image_url": "http://example.com/img/green.png"
        },
        {
            "content_type": "text",
            "title": "No",
            "payload": "{'process':'credit_card_application', 'next_step':'choose_benefit', "
                       "'data':{'previous_credit':'false'}}"
        }
    ]
    # print (outgoing_message["message"]["quick_replies"])
    send_response(outgoing_message)


def application_choose_benefit(outgoing_message, data):
    # print (data)
    outgoing_message['message']['attachment'] = {
        "type": "template",
        "payload": {
            "template_type": "generic",
            "elements": [
                {
                    "title": "Qué beneficios buscas?",
                    "image_url": "http://www.charlottestories.com/wp-content/uploads/2016/04/jimmy-johns-1-dollar-subs.jpg",
                    "subtitle": "Selecciona una de las siguientes opciones:",
                    "buttons": [
                        {
                            "type": "postback",
                            "title": "Puntos",
                            "payload": "{'process':'credit_card_application', 'next_step':'choose_benefit', "
                                       "'data':{'previous_credit':'false'}}"
                        },
                        {
                            "type": "postback",
                            "title": "Cash Back",
                            "payload": "{'process':'credit_card_application', 'next_step':'choose_benefit', "
                                       "'data':{'previous_credit':'false'}}"
                        },
                        {
                            "type": "postback",
                            "title": "Millas",
                            "payload": "menu"
                        }
                    ]
                }
            ]
        }
    }
    send_response(outgoing_message)


##############################################
#                                            #
#               Send Responses               #
#                                            #
##############################################


def send_response(outgoing_message):
    # Convert outgoing message to JSON
    response_msg = json.dumps(outgoing_message)
    # Define API URL where to deliver outgoing messages.
    # Send response via POST
    status = requests.post(POST_MESSAGE_URL, headers={"Content-Type": "application/json"}, data=response_msg)
    # Print status to the terminal
    print(status)


##############################################
#                                            #
#             Function Hash Map              #
#                                            #
##############################################

function_hash_map = {
    'credit_card_application': {
        'previous_credit': application_previous_credit,
        'choose_benefit': application_choose_benefit,
    }
}

##############################################
#                                            #
#         entry with message structure       #
#                                            #
##############################################
# this is a simple text message that is sent by the user


"""

{
    'entry': 
        [
            {
                'id': '191608155007141',
                'messaging':
                    [
                        {
                            'message':
                                {
                                    'mid': 'wbGEUpeBvNe2XWswUq1aDhK4NDpiHePdwFA2o6mvDir7Y_Po9WI-wWT5MLrVDyl7YBRJgfH6OvKiwKa62KLX8g',
                                    'seq': 1271880,
                                    'text': 'hi'
                                },
                            'recipient':
                                {
                                    'id': '191608155007141'
                                },
                            'sender':
                                {
                                    'id': '1709109715803638'
                                },
                            'timestamp': 1531290007015
                        }
                    ],
                'time': 1531290007257
            }
        ],
    'object': 'page'
}

"""

##############################################
#                                            #
#      entry with quick reply structure      #
#                                            #
##############################################
# This could be a button or any other quick reply that is sent by the user


"""

{
    'entry':
        [
            {
                'id': '191608155007141',
                'messaging':
                    [
                        {
                            'message':
                                {
                                    'mid': 'KasTF4NY8reMx7ZmQC6_YRK4NDpiHePdwFA2o6mvDiptYgNnHGpblX4hy4FlgtI_tc3DPbZ8cSVepf87m-HjJw',
                                    'quick_reply':
                                        {
                                            'payload': "{'process':'credit_card_application', "
                                                     "'next_step':'choose_benefit', "
                                                     "'data':{'previous_credit':'true'}}"
                                        },
                                    'seq': 1271919,
                                    'text': 'Si'
                                },
                            'recipient':
                                {
                                    'id': '191608155007141'
                                },
                            'sender':
                                {
                                    'id': '1709109715803638'
                                },
                            'timestamp': 1531290661636}
                    ],
                'time': 1531290661912
            }
        ],
    'object': 'page'
}

"""

##############################################
#                                            #
#       entry with postback structure        #
#                                            #
##############################################
# This is an option that the user chooses from a template that we send


"""

{   
    'entry': 
        [   
            {   
                'id': '191608155007141',
                'messaging': 
                    [   
                        {   
                            'postback': 
                                {   
                                    'payload': "{'process':'credit_card_application', 'next_step':'previous_credit', 'data':{}}",
                                    'title': 'Aplicar a Tarjetas'
                                },
                            'recipient': 
                                {   
                                    'id': '191608155007141'
                                },
                            'sender': 
                                {
                                    'id': '1709109715803638'
                                },
                            'timestamp': 1531291320470
                        }
                    ],
                'time': 1531291320470
            }
        ],
    'object': 'page'
}

"""